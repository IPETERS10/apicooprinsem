﻿using Microsoft.EntityFrameworkCore;
using Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Source
{
  public  class DAPredios
    {
        private MySqlContext _db;
        public DAPredios(MySqlContext databaseContext)
        {
            _db = databaseContext;

        }
        public JArray List(int predio)
        {
            try
            {

                //  return  list;
                return ( 
                            new JArray(
                                from p in _db.Predio where p.Id == predio
                                select new JObject(
                                    new JProperty("Codigo", p.Id),
                                    new JProperty("Nombre", p.Nombre),  
                                    new JProperty("Direccion", p.Direccion),
                                    new JProperty("Cliente", p.Cliente.Nombre), 
                                    new JProperty("Latitud", p.Latitud),
                                    new JProperty("Longitud", p.Longitud),
                                    new JProperty("Vacas",
                                        new JArray(
                                            from v in _db.Vaca
                                            where p.Id == v.PredioId
                                            select new JObject(
                                                    new JProperty("Codigo", v.Id),
                                                    new JProperty("Padre", v.Padre),
                                                    new JProperty("Madre", v.Madre),
                                                    new JProperty("FechaNacimiento", v.Nacimiento),
                                                    new JProperty("Nop", v.Nop),
                                                    new JProperty("Diio", v.DIIO),
                                                    new JProperty("Estado", v.Estado) ,
                                                    new JProperty("Reproduccion",
                                                        new JArray(
                                                                from r in _db.Reproduccion where  r.VacaId == v.Id && r.PredioId == p.Id
                                                                select new JObject(
                                                                        new JProperty("Manejo", r.Manejo),
                                                                        new JProperty("FechaManejo", r.FechaManejo),
                                                                        new JProperty("FechaUltimoParto", r.FechaUltimoParto),
                                                                        new JProperty("Ps", r.Ps),
                                                                        new JProperty("PercPro", r.PercPro) 
                                                                    )
                                                            )
                                                    
                                                   ),
                                                   new JProperty("Control",
                                                        new JArray(
                                                                from c in _db.Control where c.VacaId == v.Id && c.PredioId == p.Id 
                                                                select new JObject(
                                                                        new JProperty("Nop", c.Nop),
                                                                        new JProperty("FechaParto", c.FechaControl),
                                                                        new JProperty("FechaControl", c.FechaControl),
                                                                        new JProperty("Dia", c.Dia),
                                                                        new JProperty("Leche", c.Leche),
                                                                        new JProperty("Grasa", c.Grasa),
                                                                        new JProperty("Proteina", c.Proteina)
                                                                    )
                                                            )

                                                   )
                                            )
                                        )
                                    )
                                )
                            ) 
                        );

            }
            catch { throw; }
        }

        public object List()
        {
            try
            {

                //  return  list;
                return (
                            new JArray(
                                from p in _db.Predio 
                                select new JObject(
                                    new JProperty("Nombre", p.Nombre),
                                    new JProperty("Direccion", p.Direccion),
                                    new JProperty("Vacas",
                                        new JArray(
                                            from v in _db.Vaca
                                            where p.Id == v.PredioId
                                            select new JObject(
                                                    new JProperty("Codigo", v.Id),
                                                    new JProperty("Padre", v.Padre)
                                            )
                                        )
                                    )
                                )
                            )
                        );

            }
            catch { throw; }
        }
    }
}
