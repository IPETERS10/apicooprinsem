﻿using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Source
{
    public class DAUsuarios
    {
        private MySqlContext _db;
        public DAUsuarios(MySqlContext databaseContext)
        {
            _db = databaseContext;
            
        }
        public Task<Usuario> GetUsuarioFromDatabase(Usuario usuario) {

            try {
                return  Task.FromResult(_db.Usuario.Where(e => e.Nick == usuario.Nick.ToUpper() && e.Clave == usuario.Clave).FirstOrDefault());
            }
            catch { throw; }
        }

        public Task<bool> AddUser(Usuario usuario)
        {
            try
            {
                usuario.Nick = usuario.Nick.ToUpper();
                //valida para que no se repitan los nick o emails
                if ((_db.Usuario.Where(e=>e.Nick == usuario.Nick || e.Email == usuario.Email).FirstOrDefault()) == null)
                {
                    _db.Usuario.Add(usuario);
                    _db.SaveChanges();
                    return Task.FromResult(true);
                }
                else {
                    return Task.FromResult(false);
                }
               
            }
            catch { return Task.FromResult(false); }
           
        }
    }
}
