﻿using Microsoft.EntityFrameworkCore;
using Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Source
{
    public class DAOpciones
    {
        private MySqlContext _db;
        public DAOpciones(MySqlContext databaseContext)
        {
            _db = databaseContext;

        }
        public Task<JArray> GetOpcionesUsuario(Usuario usuario)
        {
            try
            {
                var clientes = (from usu in _db.Usuario.Where(e => e.Nick == usuario.Nick)
                                join vis in _db.Visualizador on usu.Id equals vis.UsuarioId
                                join acc in _db.Acceso on vis.Id equals acc.VisualizadorId
                                join opt in _db.Opcion on acc.OpcionId equals opt.Id
                                join cli in _db.Cliente on vis.ClienteId equals cli.Id
                                join pred in _db.Predio
                                on new { PredioId = vis.PredioId, Cliente = cli.Id }
                                equals new { PredioId = pred.Id, Cliente = pred.ClienteId }
                                select cli).Distinct().ToList();
                var predios = (from usu in _db.Usuario.Where(e => e.Nick == usuario.Nick)
                               join vis in _db.Visualizador on usu.Id equals vis.UsuarioId
                               join acc in _db.Acceso on vis.Id equals acc.VisualizadorId
                               join opt in _db.Opcion on acc.OpcionId equals opt.Id
                               join cli in _db.Cliente on vis.ClienteId equals cli.Id
                               join pred in _db.Predio
                               on new { PredioId = vis.PredioId, Cliente = cli.Id }
                               equals new { PredioId = pred.Id, Cliente = pred.ClienteId }
                               select pred).Distinct().ToList();
                var opciones = (from usu in _db.Usuario.Where(e => e.Nick == usuario.Nick)
                                join vis in _db.Visualizador on usu.Id equals vis.UsuarioId
                                join acc in _db.Acceso on vis.Id equals acc.VisualizadorId
                                join opt in _db.Opcion on acc.OpcionId equals opt.Id
                                join cli in _db.Cliente on vis.ClienteId equals cli.Id
                                join pred in _db.Predio
                                on new { PredioId = vis.PredioId, Cliente = cli.Id }
                                equals new { PredioId = pred.Id, Cliente = pred.ClienteId }
                                select opt).Distinct().ToList();

                return Task.FromResult((new JArray(
                    from cli in clientes
                    select new JObject(
                        new JProperty("Rut", cli.Rut),
                        new JProperty("Nombre", cli.Nombre),
                        new JProperty("Sap", cli.Sap),
                        new JProperty("Email", cli.Email),
                        new JProperty("Predios", from pre in predios
                                                 select new JObject(
                                                new JProperty("Nombre", pre.Nombre),
                                                new JProperty("Direccion", pre.Direccion),
                                                new JProperty("Latitud", pre.Latitud),
                                                new JProperty("Longitud", pre.Longitud),
                                                new JProperty("Accesos", from opt in opciones
                                                                         select new JObject(
                                                                         new JProperty("Opcion", opt.Nombre),
                                                                          new JProperty("Titulo", opt.Nombre),
                                                                           new JProperty("Descripcion", opt.Descripcion),
                                                                            new JProperty("Icon", opt.Icon),
                                                                             new JProperty("Orden", opt.Orden)))
                                            ))
                       )
                    )));
            }
            catch { throw; }
        }

       

    }
}
