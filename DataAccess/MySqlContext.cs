﻿using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class MySqlContext : DbContext
    {
        public MySqlContext(DbContextOptions<MySqlContext> options)
        : base(options)
        {
           
        }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<Acceso> Acceso { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Modulo> Modulo { get; set; }
        public virtual DbSet<Opcion> Opcion { get; set; }
        public virtual DbSet<Predio> Predio { get; set; }
        public virtual DbSet<Visualizador> Visualizador { get; set; }
        public virtual DbSet<Toro> Toro { get; set; }
        public virtual DbSet<Vaca> Vaca { get; set; }
        public virtual DbSet<IndicadorEd1p> IndicadorEd1p { get; set; }
        public virtual DbSet<IndicadorLip> IndicadorLip { get; set; }
        public virtual DbSet<IndicadorLp1s> IndicadorLp1s { get; set; }
        public virtual DbSet<IndicadorLpp> IndicadorLpp { get; set; }
        public virtual DbSet<Reproduccion> Reproduccion { get; set; }
        public virtual DbSet<ResumenControl> ResumenControl { get; set; }
        public virtual DbSet<ResumenCurvaLactancia> ResumenCurvaLactancia { get; set; }
        public virtual DbSet<ResumenExistenciaCrias> ResumenExistenciaCrias { get; set; }
        public virtual DbSet<ResumenExistenciaVacas> ResumenExistenciaVacas { get; set; }
        public virtual DbSet<ResumenLactancia> ResumenLactancia { get; set; }
        public virtual DbSet<ResumenReproductivo> ResumenReproductivo { get; set; }
        public virtual DbSet<Control> Control { get; set; }



    }
}
