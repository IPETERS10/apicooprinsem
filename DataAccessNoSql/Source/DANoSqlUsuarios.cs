﻿using DataAccess;
using DataAccessNoSql.MongoModels;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessNoSql.Source
{
    public class DANoSqlUsuarios
    {
        private MongoDbContext _db;
        public DANoSqlUsuarios()
        {
            _db = new MongoDbContext();
        }
        public Task<JArray> ListarClientesPorUsuario(int rutUsuario)
        {
            try
            {
                var filter = Builders<MonUsuarios>.Filter.Eq("rut", rutUsuario);
                var clientes = _db.Usuarios.Find(filter).FirstOrDefault().Clientes;

                    return Task.FromResult(new JArray {
                    from cli in
                        clientes select new JObject(
                             new JProperty("id",cli["cliente_id"].ToString()),
                             new JProperty("rut",cli["rut"].ToString()),
                             new JProperty("nombre",cli["nombre"].ToString())
                           )
                   });
            }
            catch { throw; }
        }
        public Task<JArray> ListarUsuarios()
        {
            try
            {

                var users = _db.Usuarios.AsQueryable().ToList();
                JArray json;
               // consulta si el usuario tiene clientes
                  if (users.Count() > 0)
                {
                    json = new JArray {
                      from cli in
                          users select new JObject(
                               new JProperty("rut",cli.Rut),
                               new JProperty("nick",cli.Nick),
                               new JProperty("clientes",cli.Clientes.Count)
                             )
                     };
                }
                else
                {
                    json = null;
                }
                return Task.FromResult(json);
            }
            catch { throw; }
        }



        public Task<object> GetUsuario(int rut)
        {
            object response = null;
            var filter = Builders<MonUsuarios>.Filter.Eq("rut", rut);
            var user = _db.Usuarios.Find(filter).FirstOrDefault();
            var builderClient = Builders<MonClientes>.Filter;
            var clientes = new List<MonClientes>();
            var builderPredios = Builders<MonPredios>.Filter;
            foreach (var cli in user.Clientes) {
                clientes.Add(_db.Clientes.Find(builderClient.Eq("_id", cli["cliente_id"])).FirstOrDefault());
            }
            if (user != null)
            {
                return Task.FromResult((object)new JObject(
                    new JProperty("id", user.Id.ToString()),
                     new JProperty("nick", user.Nick),
                     new JProperty("rut", user.Rut),
                    new JProperty("clientes", from cli in
                       clientes select new JObject(
                           
                                                  new JProperty("cliente_id", cli.Id.ToString()),
                                                  new JProperty("nombre", cli.Nombre),
                                                  new JProperty("rut", cli.Rut.ToString()),
                                                  //se le pasa el rut del usuario para saber si puede o no ver ese predio
                                                 new JProperty("predios", BuscarPredios(user.Rut,cli.Predios))
                                        )
                                )));
            }
            dynamic BuscarPredios(int userRut, BsonArray arrPredios)
            {
                var arrJObj = new JArray();
                List<MonPredios> listPredios = new List<MonPredios>();
                foreach (var pre in arrPredios) {
                    listPredios.Add(_db.Predios.Find(builderPredios.Eq("_id", pre["predio_id"].AsObjectId)).FirstOrDefault());
                }
                foreach (var predio in listPredios) {
                    arrJObj.Add(new JObject(
                        new JProperty("id_predio", predio.Id.ToString()),
                         new JProperty("nombre", predio.Nombre),
                          new JProperty("latitud", predio.Latitud),
                           new JProperty("longitud", predio.Longitud),
                         new JProperty("accesos", BuscarAccesosEnPredio(userRut, predio.Accesos))
                        ));
                }
                if (!(listPredios.Count > 0))
                    return "no tiene predios";

                return arrJObj;

            }
            dynamic BuscarAccesosEnPredio(int userRut, BsonArray accesos) {
                var arrJObj = new JObject();
                foreach (var acc in accesos) {
                    if (acc["usuario_rut"].ToInt32() == userRut) {
                        arrJObj.Add(
                            new JProperty("opciones", from opt in acc["opciones"].AsBsonArray select new JObject(
                                    new JProperty("id_opcion",opt["id_opcion"].ToString()),
                                  new JProperty("titulo", opt["titulo"].ToString()),
                                    new JProperty("descripcion", opt["descripcion"].ToString()),
                                      new JProperty("icon", opt["icon"].ToString())
                                ))
                            );
                    }
                }
                if (arrJObj.Count > 0)
                    return arrJObj;
                else
                    return "no tiene accesos sobre este predio";
            }
            return Task.FromResult(response);
        }

        public Task<dynamic> GetClienteDelUsuario(int rutUsuario, int rutCliente)
        {
            object response = null;
            var filter = Builders<MonUsuarios>.Filter.Eq("rut", rutUsuario);
            var clientes = _db.Usuarios.Find(filter).FirstOrDefault().Clientes;
            if (clientes != null)
            {
                foreach (var cli in clientes)
                {
                    if (cli["rut"].ToInt32() == rutCliente)
                    {
                        var filterCli = Builders<MonClientes>.Filter.Eq("rut", rutCliente);
                        var cliente = _db.Clientes.Find(filterCli).FirstOrDefault();
                        return Task.FromResult((object)new JObject(
                                  new JProperty("cliente_id", cliente.Id.ToString()),
                                  new JProperty("nombre", cliente.Nombre),
                                  new JProperty("rut", cliente.Rut),
                                  new JProperty("predios", cliente.Predios.Count)
                                  ));
                    }
                }

            }

            return Task.FromResult(response);
        }
        public Task<JArray> GetPrediosDelCliente(int rutUsuario, int rutCliente)
        {
            try
            {
              
                var filter = Builders<MonUsuarios>.Filter.Eq("rut", rutUsuario);
                var usuario = _db.Usuarios.Find(filter).FirstOrDefault();

                if (usuario != null)
                {
                    foreach (var cli in usuario.Clientes)
                    {
                        if (cli["rut"].ToInt32() == rutCliente)
                        {
                            var filterPredio = Builders<MonPredios>.Filter.Eq("cliente", rutCliente);
                            var predios = _db.Predios.Find(filterPredio).ToList();
                            return Task.FromResult(new JArray( from predio in predios select new JObject(
                                      new JProperty("predio_id", predio.Id.ToString()),
                                      new JProperty("nombre", predio.Nombre),
                                      new JProperty("direccion", predio.Direccion),
                                      new JProperty("latitud", predio.Latitud),
                                      new JProperty("longitud", predio.Longitud)
                                      )));
                        }
                    }

                }

                return null;
            }
            catch { throw; }
           
        }
        public Task<bool> AddUser(MonUsuarios usuario)
        {
            try
            {
                _db.Usuarios.InsertOne(usuario);

                return Task.FromResult(true);
            }
            catch { return Task.FromResult(false); }

        }
    }
}
