﻿using DataAccessNoSql.MongoModels;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class MongoDbContext
    {
        //si no se le pasa ninguna cadena de conexion automaticamente se conecta al localhost
        private readonly IMongoDatabase _database = null;
        public MongoDbContext()
        {
            //"mongodb://localhost:27017"
            var client = new MongoClient();
             if (client != null)
                _database = client.GetDatabase("coopri");
        }

        public IMongoCollection<MonUsuarios> Usuarios
        {
            get
            {
                return _database.GetCollection<MonUsuarios>("usuarios");
            }
        }
        public IMongoCollection<MonClientes> Clientes
        {
            get
            {
                return _database.GetCollection<MonClientes>("clientes");
            }
        }
        public IMongoCollection<MonPredios> Predios
        {
            get
            {
                return _database.GetCollection<MonPredios>("predios");
            }
        }
    }
}
