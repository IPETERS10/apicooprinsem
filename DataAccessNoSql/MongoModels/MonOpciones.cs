﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessNoSql.MongoModels
{
    public class MonOpciones
    {

        public ObjectId Id { get; set; }
        [BsonElement("titulo")]
        [BsonIgnoreIfNull]
        public string Titulo { get; set; }
        [BsonIgnoreIfNull]
        [BsonElement("descripcion")]
        public string Descripcion { get; set; }
        [BsonElement("icon")]
        public string Icon { get; set; }
    }
}
