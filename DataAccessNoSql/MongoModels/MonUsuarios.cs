﻿using DataAccessNoSql.MongoModels;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessNoSql.MongoModels
{
  
    public class MonUsuarios
    {
        public ObjectId Id { get; set; }
        [BsonElement("rut")]
        public int Rut { get; set; }
        [BsonElement("nick")]
        public string Nick { get; set; }
        [BsonElement("clientes")]
        public BsonArray Clientes { get; set; }
    }

    }
