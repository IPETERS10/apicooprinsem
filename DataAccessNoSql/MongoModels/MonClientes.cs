﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessNoSql.MongoModels
{

    public class MonClientes
    {
        [BsonIgnoreIfNull]
        public ObjectId Id { get; set; }
        [BsonElement("rut")]
        [BsonIgnoreIfNull]
        public int Rut { get; set; }
        [BsonElement("nombre")]
        public string Nombre { get; set; }
        [BsonIgnoreIfNull]
        [BsonElement("predios")]
        public BsonArray Predios { get; set; }
    }

}
