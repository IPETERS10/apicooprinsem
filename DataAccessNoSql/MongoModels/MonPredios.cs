﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessNoSql.MongoModels
{
    public class MonPredios
    {

        public ObjectId Id { get; set; }
        [BsonElement("nombre")]
        public string Nombre { get; set; }
        [BsonElement("direccion")]
        public string Direccion { get; set; }
        [BsonIgnoreIfNull]
        [BsonElement("latitud")]
        public double Latitud { get; set; }
        [BsonIgnoreIfNull]
        [BsonElement("longitud")]
        public double Longitud { get; set; }
        [BsonElement("cliente")]
        public int ClienteRut { get; set; }
        [BsonElement("accesos")]
        public BsonArray Accesos { get; set; }
    }
}
