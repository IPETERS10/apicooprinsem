﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
  
    public class Predio
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int ClienteId { get; set; }
        [ForeignKey("ClienteId")]
        public virtual Cliente Cliente { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public double? Latitud { get; set; }
        public double? Longitud { get; set; }

        public virtual List<Visualizador> Visualizadores { get; set; } = new List<Visualizador>();
        public virtual List<Vaca> Vacas { get; set; } = new List<Vaca>();
        public virtual List<Toro> Toro { get; set; } = new List<Toro>();
        public virtual List<IndicadorEd1p> IndicadorEd1p { get; set; } = new List<IndicadorEd1p>();
        public virtual List<IndicadorLip> IndicadorLip { get; set; } = new List<IndicadorLip>();
        public virtual List<IndicadorLp1s> IndicadorLp1s { get; set; } = new List<IndicadorLp1s>();
        public virtual List<IndicadorLpp> IndicadorLpp { get; set; } = new List<IndicadorLpp>();
        public virtual List<Reproduccion> Reproduccion { get; set; } = new List<Reproduccion>();
        public virtual List<ResumenControl> ResumenControl { get; set; } = new List<ResumenControl>();
        public virtual List<ResumenCurvaLactancia> ResumenCurvaLactancia { get; set; } = new List<ResumenCurvaLactancia>();
        public virtual List<ResumenExistenciaCrias> ResumenExistenciaCrias { get; set; } = new List<ResumenExistenciaCrias>();
        public virtual List<ResumenExistenciaVacas> ResumenExistenciaVacas { get; set; } = new List<ResumenExistenciaVacas>();

        public virtual List<ResumenLactancia> ResumenLactancia { get; set; } = new List<ResumenLactancia>();

        public virtual List<ResumenReproductivo> ResumenReproductivo { get; set; } = new List<ResumenReproductivo>();

        public virtual List<Control> Control { get; set; } = new List<Control>();




    }
}
