﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    public class Control
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int VacaId { get; set; }
        [ForeignKey("VacaId")]
        public virtual Vaca Vaca { get; set; }
        public int PredioId { get; set; }
        [ForeignKey("PredioId")]
        public virtual Predio Predio { get; set; }

        public int Nop { get; set; }

        public DateTime FechaParto { get; set; }
        public DateTime FechaControl { get; set; }
        public int Dia { get; set; }
        public double? Leche { get; set; }
        public double? Grasa { get; set; }
        public double? Proteina { get; set; }



    }
}