﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    public class ResumenLactancia
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int PredioId { get; set; }
        [ForeignKey("PredioId")]
        public virtual Predio Predio { get; set; }

        public string Variable { get; set; }
        public double? NopUno { get; set; }
        public double? NopDos { get; set; }

        public double? NopTres { get; set; }


    }
}
