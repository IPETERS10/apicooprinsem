﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
  
    public class Modulo
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public string Nombre  { get; set; }
        public int Tecnologia { get; set; }
        public int Orden { get; set; }
        public string Icono { get; set; }
        public int Descripcion { get; set; }
        public string Titulo { get; set; }
        public virtual List<Opcion> Opciones { get; set; } = new List<Opcion>();
    }
}
