﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    public class Reproduccion
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int VacaId { get; set; }
        [ForeignKey("VacaId")]
        public virtual Vaca Vaca { get; set; }
        public string Manejo { get; set; }

        public DateTime FechaManejo { get; set; }
        public DateTime FechaUltimoParto { get; set; }
        public int Ps { get; set; }
        public int PercPro { get; set; } 
        public int PredioId { get; set; }
        [ForeignKey("PredioId")]
        public virtual Predio Predio { get; set; }
    }
}