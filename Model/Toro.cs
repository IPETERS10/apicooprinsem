﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
  
   public class Toro
    {

        [Key]
        [Column("Id")]
        public string Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; } 
        public string Completo { get; set; }
        public int PredioId { get; set; }
        [ForeignKey("PredioId")] 
        public virtual Predio Predio { get; set; } 

    }
}
