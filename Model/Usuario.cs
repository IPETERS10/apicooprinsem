﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
  
    public class Usuario
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        [Required]
        public string Nick { get; set; }
        public int Rut { get; set; }
        [Required]
        public string Clave { get; set; }
        public string Rol { get; set; }
        public string Email { get; set; }
        public int Fono { get; set; }
        public int Estado { get; set; }
     
        public virtual List<Visualizador> Visualizadores { get; set; } = new List<Visualizador>();
    }
}
