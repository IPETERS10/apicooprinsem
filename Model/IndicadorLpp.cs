﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    public class IndicadorLpp
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int Rango { get; set; }
        public int Frecuencia { get; set; }
        public int PredioId { get; set; }
        [ForeignKey("PredioId")]
        public virtual Predio Predio { get; set; }
    }
}