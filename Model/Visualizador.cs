﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
  
    public class Visualizador
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int ClienteId { get; set; }
        [ForeignKey("ClienteId")]
        public virtual Cliente Cliente { get; set; }
        public int UsuarioId { get; set; }
        [ForeignKey("UsuarioId")]
        public virtual Usuario Usuario { get; set; }
        public virtual int PredioId { get; set; }
        [ForeignKey("PredioId")]
        public virtual Predio Predio { get; set; }
        public virtual List<Acceso> Accesos { get; set; } = new List<Acceso>();
    }
}
