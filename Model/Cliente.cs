﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
  
    public class Cliente
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Rut { get; set; }
        public string Direccion { get; set; }
        public int Sap { get; set; }
        public string Email { get; set; }
        public int Fono { get; set; }
        public virtual List<Visualizador> Visualizadores { get; set; } = new List<Visualizador>();
        public virtual List<Predio> Predios { get; set; } = new List<Predio>();
    }
}
