﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    public class ResumenCurvaLactancia
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int PredioId { get; set; }
        [ForeignKey("PredioId")]
        public virtual Predio Predio { get; set; }

        public int Dias { get; set; }
        public int Otono { get; set; }
        public int Primavera { get; set; } 


    }
}
