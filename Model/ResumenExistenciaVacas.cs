﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    public class ResumenExistenciaVacas
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int PredioId { get; set; }
        [ForeignKey("PredioId")]
        public virtual Predio Predio { get; set; }

        public string Variable { get; set; }
        public int Numero { get; set; } 


    }
}
