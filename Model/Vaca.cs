﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
 
    public class Vaca
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public DateTime Nacimiento { get; set; }
        public string Padre { get; set; } 
        public int Madre { get; set; }
        public int Nop { get; set; }
        public int DIIO { get; set; }
        public int PredioId { get; set; }
        [ForeignKey("PredioId")]
        public virtual Predio Predio { get; set; }
        public string Estado { get; set; }
        public virtual List<Reproduccion> Reproduccion { get; set; } = new List<Reproduccion>();
        public virtual List<Control> Control { get; set; } = new List<Control>();

    }
}
