﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
  
    public class Acceso
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int VisualizadorId { get; set; }
        [ForeignKey("VisualizadorId")]
        public virtual Visualizador Visualizador { get; set; }
        public int OpcionId { get; set; }
        [ForeignKey("OpcionId")]
        public virtual Opcion Opcion { get; set; }
        public int Estado { get; set; }
    }
}
