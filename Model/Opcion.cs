﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
  
    public class Opcion
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int ModuloId { get; set; }
        [ForeignKey("ModuloId")]
        public virtual Modulo Modulo { get; set; }
        public string Nombre  { get; set; }
        public int Orden { get; set; }
        public string Descripcion { get; set; }
        public string Icon { get; set; }
        public virtual List<Acceso> Accesos { get; set; } = new List<Acceso>();

    }
}
