-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: cooprinsem
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acceso`
--

DROP TABLE IF EXISTS `acceso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acceso` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `VisualizadorId` int(11) DEFAULT NULL,
  `OpcionId` int(11) DEFAULT NULL,
  `Estado` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_OPCION_ACCESO_ACCESO_ID` (`OpcionId`),
  KEY `FK_VISUALIZADOR_ACCESO_VISUALIZADOR_ID` (`VisualizadorId`),
  CONSTRAINT `FK_OPCION_ACCESO_ACCESO_ID` FOREIGN KEY (`OpcionId`) REFERENCES `opcion` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_VISUALIZADOR_ACCESO_VISUALIZADOR_ID` FOREIGN KEY (`VisualizadorId`) REFERENCES `visualizador` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acceso`
--

LOCK TABLES `acceso` WRITE;
/*!40000 ALTER TABLE `acceso` DISABLE KEYS */;
INSERT INTO `acceso` VALUES (1,1,2,1),(2,1,1,1),(3,2,2,1),(4,2,1,1),(5,4,1,1),(6,3,1,1),(7,5,2,1),(8,5,1,1);
/*!40000 ALTER TABLE `acceso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(80) DEFAULT NULL,
  `Rut` int(11) DEFAULT NULL,
  `Direccion` varchar(60) DEFAULT NULL,
  `Sap` int(11) NOT NULL,
  `Email` varchar(60) DEFAULT NULL,
  `Fono` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'Ivan Andrés Peters Vera',16112346,'Su Casa',400014522,'ivan.peters.v@gmail.com',952094319),(2,'Jorge Willer WIller',999999,'Su Casa 3.0',100015210,'jorge.willer@mail.com',7412852);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `control`
--

DROP TABLE IF EXISTS `control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `VacaId` int(11) DEFAULT NULL,
  `PredioId` int(11) DEFAULT NULL,
  `Nop` int(11) DEFAULT NULL,
  `FechaParto` date DEFAULT NULL,
  `FechaControl` date DEFAULT NULL,
  `Dia` int(11) DEFAULT NULL,
  `Leche` double(5,2) DEFAULT NULL,
  `Grasa` double(5,2) DEFAULT NULL,
  `Proteina` double(5,2) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_CONTROL_PREDIO_ID_idx` (`PredioId`),
  KEY `FK_VACA_CONTROL_VACA_ID_idx` (`VacaId`),
  CONSTRAINT `FK_PREDIO_CONTROL_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_VACA_CONTROL_VACA_ID` FOREIGN KEY (`VacaId`) REFERENCES `vaca` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `control`
--

LOCK TABLES `control` WRITE;
/*!40000 ALTER TABLE `control` DISABLE KEYS */;
INSERT INTO `control` VALUES (1,1831,1,8,'2015-08-28','2015-09-25',29,24.80,4.88,3.22),(2,1831,1,8,'2015-08-28','2015-10-22',56,23.20,3.67,2.79),(3,1831,1,8,'2015-08-28','2015-11-27',92,16.80,4.17,2.95),(4,1831,1,8,'2015-08-28','2015-12-31',126,24.40,4.22,2.96),(5,1831,1,8,'2015-08-28','2016-01-28',154,13.70,4.40,3.08),(6,1831,1,8,'2015-08-28','2016-02-25',182,18.40,4.61,3.40),(7,1831,1,8,'2015-08-28','2016-03-28',214,18.00,4.87,3.29),(8,1831,1,8,'2015-08-28','2016-04-29',246,14.40,5.85,3.82),(9,1831,1,8,'2015-08-28','2016-05-30',277,17.20,5.15,3.73),(10,1937,1,7,'2016-05-06','2016-05-29',24,19.20,3.38,4.08),(11,1937,1,7,'2016-05-06','2016-06-28',54,23.60,4.12,3.02),(12,1937,1,7,'2016-05-06','2017-07-30',451,25.60,3.43,3.18),(13,1937,1,7,'2016-05-06','2016-08-29',116,15.20,4.71,4.44),(14,1937,1,7,'2016-05-06','2016-09-25',143,28.53,2.81,3.06),(15,1937,1,7,'2016-05-06','2016-10-22',170,27.20,3.64,3.00),(16,1937,1,7,'2016-05-06','2016-11-29',208,18.53,4.16,2.98),(17,1937,1,7,'2016-05-06','2016-12-29',238,25.20,2.86,3.45),(18,1937,1,7,'2016-05-06','2017-01-30',270,22.89,4.39,3.46),(19,1937,1,7,'2016-05-06','2017-02-25',296,26.80,5.27,3.33),(20,1937,1,7,'2016-05-06','2017-03-28',327,22.80,4.88,3.48),(21,1937,1,7,'2016-05-06','2017-04-28',358,20.00,5.46,3.70),(22,1937,1,7,'2016-05-06','2017-05-30',390,23.60,5.21,4.06),(23,2510,1,6,'2015-11-05','2015-11-28',24,38.00,4.14,3.25),(24,2510,1,6,'2015-11-05','2015-12-29',55,26.40,2.82,3.48),(25,2510,1,6,'2015-11-05','2016-01-30',87,22.40,1.99,3.50),(26,2510,1,6,'2015-11-05','2016-02-28',116,23.60,4.22,3.27),(27,2510,1,6,'2015-11-05','2016-03-29',146,19.60,3.81,3.37),(28,2510,1,6,'2015-11-05','2017-04-30',543,19.60,4.79,3.27),(29,2510,1,6,'2015-11-05','2016-05-25',203,28.00,3.86,3.32),(30,2510,1,6,'2015-11-05','2016-06-29',238,27.20,2.95,3.09),(31,2510,1,6,'2015-11-05','2016-07-28',267,28.40,3.04,3.45),(32,2510,1,6,'2015-11-05','2016-08-30',300,28.40,3.18,3.68),(33,2510,1,6,'2015-11-05','2016-09-30',331,32.86,3.99,3.44),(34,2510,1,6,'2015-11-05','2016-10-25',356,26.40,3.03,3.51),(35,2510,1,6,'2015-11-05','2016-11-29',391,31.60,4.72,3.36),(36,2510,1,6,'2015-11-05','2016-12-28',420,26.40,4.83,3.43),(37,2510,1,6,'2015-11-05','2017-01-30',453,31.60,3.08,3.70),(38,2442,1,6,'2016-04-24','2016-06-22',60,30.00,4.04,3.46),(39,2442,1,6,'2016-04-24','2016-07-29',97,30.00,3.32,2.77),(40,2442,1,6,'2016-04-24','2016-08-30',129,24.00,3.89,3.12),(41,2442,1,6,'2016-04-24','2016-09-29',159,32.00,3.74,3.64),(42,2442,1,6,'2016-04-24','2016-10-25',185,24.78,3.29,3.72),(43,2442,1,6,'2016-04-24','2016-11-29',220,23.54,3.62,3.63),(44,2442,1,6,'2016-04-24','2016-12-28',249,20.27,2.21,3.82),(45,2442,1,6,'2016-04-24','2017-01-30',282,20.12,3.50,3.44),(46,2442,1,6,'2016-04-24','2016-06-22',60,26.00,2.96,3.48),(47,2442,1,6,'2016-04-24','2016-07-29',97,23.60,3.17,2.87),(48,2442,1,6,'2016-04-24','2016-08-30',129,28.00,2.80,3.02),(49,2442,1,6,'2016-04-24','2016-09-29',159,28.40,3.38,3.50),(50,2442,1,6,'2016-04-24','2016-10-25',185,19.52,3.40,3.27),(51,2442,1,6,'2016-04-24','2016-10-25',185,38.00,2.69,3.48),(52,2442,1,6,'2016-04-24','2016-11-29',220,21.20,3.09,3.43),(53,2442,1,6,'2016-04-24','2016-12-28',249,15.20,1.87,3.38);
/*!40000 ALTER TABLE `control` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indicadored1p`
--

DROP TABLE IF EXISTS `indicadored1p`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indicadored1p` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PredioId` int(11) DEFAULT NULL,
  `Rango` int(11) DEFAULT NULL,
  `Otono` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_ED1P_PREDIO_ID_idx` (`PredioId`),
  CONSTRAINT `FK_PREDIO_ED1P_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indicadored1p`
--

LOCK TABLES `indicadored1p` WRITE;
/*!40000 ALTER TABLE `indicadored1p` DISABLE KEYS */;
INSERT INTO `indicadored1p` VALUES (1,1,20,4),(2,1,22,19),(3,1,24,34),(4,1,26,32),(5,1,28,19),(6,1,30,15),(7,1,32,18),(8,1,34,9),(9,1,36,4),(10,1,38,2),(11,1,40,1);
/*!40000 ALTER TABLE `indicadored1p` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indicadorlip`
--

DROP TABLE IF EXISTS `indicadorlip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indicadorlip` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PredioId` int(11) DEFAULT NULL,
  `Rango` int(11) DEFAULT NULL,
  `Frecuencia` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_LIP_PREDIO_ID_idx` (`PredioId`),
  CONSTRAINT `FK_PREDIO_LIP_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indicadorlip`
--

LOCK TABLES `indicadorlip` WRITE;
/*!40000 ALTER TABLE `indicadorlip` DISABLE KEYS */;
INSERT INTO `indicadorlip` VALUES (1,1,320,0),(2,1,335,1),(3,1,350,4),(4,1,365,15),(5,1,380,46),(6,1,395,42),(7,1,410,35),(8,1,425,30),(9,1,440,16),(10,1,455,9),(11,1,470,3);
/*!40000 ALTER TABLE `indicadorlip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indicadorlp1s`
--

DROP TABLE IF EXISTS `indicadorlp1s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indicadorlp1s` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PredioId` int(11) DEFAULT NULL,
  `Rango` int(11) DEFAULT NULL,
  `Frecuencia` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_LP1S_PREDIO_ID_idx` (`PredioId`),
  CONSTRAINT `FK_PREDIO_LP1S_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indicadorlp1s`
--

LOCK TABLES `indicadorlp1s` WRITE;
/*!40000 ALTER TABLE `indicadorlp1s` DISABLE KEYS */;
INSERT INTO `indicadorlp1s` VALUES (1,1,20,19),(2,1,32,23),(3,1,44,25),(4,1,56,24),(5,1,68,19),(6,1,80,22),(7,1,92,21),(8,1,104,17),(9,1,116,14),(10,1,128,12),(11,1,140,8);
/*!40000 ALTER TABLE `indicadorlp1s` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indicadorlpp`
--

DROP TABLE IF EXISTS `indicadorlpp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indicadorlpp` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PredioId` int(11) DEFAULT NULL,
  `Rango` int(11) DEFAULT NULL,
  `Frecuencia` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_LPP_PREDIO_ID_idx` (`PredioId`),
  CONSTRAINT `FK_PREDIO_LPP_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indicadorlpp`
--

LOCK TABLES `indicadorlpp` WRITE;
/*!40000 ALTER TABLE `indicadorlpp` DISABLE KEYS */;
INSERT INTO `indicadorlpp` VALUES (1,1,35,3),(2,1,52,6),(3,1,69,12),(4,1,86,35),(5,1,103,43),(6,1,120,51),(7,1,137,31),(8,1,154,27),(9,1,171,12),(10,1,188,4),(11,1,205,2);
/*!40000 ALTER TABLE `indicadorlpp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo`
--

DROP TABLE IF EXISTS `modulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Tecnologia` smallint(6) DEFAULT NULL,
  `Orden` smallint(6) DEFAULT NULL,
  `Icono` varchar(50) DEFAULT NULL,
  `Descripcion` text,
  `Titulo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo`
--

LOCK TABLES `modulo` WRITE;
/*!40000 ALTER TABLE `modulo` DISABLE KEYS */;
INSERT INTO `modulo` VALUES (1,'CLIWin',0,NULL,'add shopping cart','Este módulo permitirá sincronizar toda la información de sus sistemas CliWin directamente con todos sus dispostivos que esten autorizados para visuzalizar el contenido CLIWIN','Acceso Sistema CliWin');
/*!40000 ALTER TABLE `modulo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcion`
--

DROP TABLE IF EXISTS `opcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opcion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ModuloId` int(11) DEFAULT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Orden` smallint(6) DEFAULT NULL,
  `Descripcion` text,
  `Icon` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ModuloId` (`ModuloId`),
  CONSTRAINT `opcion_ibfk_1` FOREIGN KEY (`ModuloId`) REFERENCES `modulo` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcion`
--

LOCK TABLES `opcion` WRITE;
/*!40000 ALTER TABLE `opcion` DISABLE KEYS */;
INSERT INTO `opcion` VALUES (1,1,'Importa Predios',1,'Permite Importar predios directamente desde interfaz windows para API de coneccion de plataforma COOPRINSEM','compare arrows'),(2,1,'Visualizar Vacas',2,'Visualza informacion de vacas del clientes, donde se pueden detallar controles de parto, cubiertas, preñeces, etc','receipt');
/*!40000 ALTER TABLE `opcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predio`
--

DROP TABLE IF EXISTS `predio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `predio` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) DEFAULT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Direccion` varchar(50) DEFAULT NULL,
  `Latitud` double DEFAULT NULL,
  `Longitud` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ClienteId` (`ClienteId`),
  CONSTRAINT `predio_ibfk_1` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predio`
--

LOCK TABLES `predio` WRITE;
/*!40000 ALTER TABLE `predio` DISABLE KEYS */;
INSERT INTO `predio` VALUES (1,1,'Los Boldos','Camino Ruta 14522',NULL,NULL),(2,1,'Los Pellines','Camino plagados de pellines',NULL,NULL),(3,2,'Los Alarces','Ruta 5 Sur',NULL,NULL),(4,2,'Las Araucarias','camino Purranque',NULL,NULL);
/*!40000 ALTER TABLE `predio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reproduccion`
--

DROP TABLE IF EXISTS `reproduccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reproduccion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `VacaId` int(11) DEFAULT NULL,
  `Manejo` varchar(45) DEFAULT NULL,
  `FechaManejo` date DEFAULT NULL,
  `FechaUltimoParto` date DEFAULT NULL,
  `Ps` int(11) DEFAULT NULL,
  `PercPro` int(11) DEFAULT NULL,
  `PredioId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_VACA_REPRODUCCION_VACA_ID_idx` (`VacaId`),
  KEY `FK_PREDIO_REPRODUCCION_PREDIO_ID_idx` (`PredioId`),
  CONSTRAINT `FK_PREDIO_REPRODUCCION_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_VACA_REPRODUCCION_VACA_ID` FOREIGN KEY (`VacaId`) REFERENCES `vaca` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reproduccion`
--

LOCK TABLES `reproduccion` WRITE;
/*!40000 ALTER TABLE `reproduccion` DISABLE KEYS */;
INSERT INTO `reproduccion` VALUES (70,1831,'a Eliminar','2016-03-03','2015-08-28',59,50,1),(71,1937,'a Eliminar','2016-03-03','2016-05-06',61,14,1),(72,2510,'a Eliminar','2016-11-12','2015-11-05',92,32,1),(73,2442,'a Secar','2017-06-13','2016-04-24',64,34,1),(74,2191,'a Cubrir','2017-01-08','2016-11-24',87,36,1),(75,2642,'a Eliminar','2016-03-03','2016-10-24',40,58,1),(76,2657,'a Cubrir','2016-11-30','2016-10-16',73,24,1),(77,2664,'a Parir','2017-02-04','2015-09-28',112,30,1),(78,2667,'a Cubrir','2016-10-25','2016-09-10',61,58,1),(79,2678,'a Eliminar','2016-03-03','2016-05-06',80,39,1),(80,4218,'a Eliminar','2016-03-03','2016-06-06',64,2,1),(81,2687,'a Eliminar','2016-03-03','2016-01-22',99,86,1),(92,2787,'a Cubrir','2017-02-08','2016-09-27',49,95,1),(93,2774,'a Cubrir','2016-07-21','2016-01-22',99,82,1),(154,2823,'a Secar','2017-07-05','2016-03-12',63,93,1),(155,2832,'a Cubrir','2016-11-07','2016-09-23',27,77,1),(156,2843,'a Eliminar','2016-11-12','2016-11-13',59,27,1),(157,2849,'a Parir','2017-03-28','2015-11-08',66,11,1),(158,2878,'a Eliminar','2016-11-12','2016-04-29',97,84,1),(159,2872,'a Eliminar','2016-03-03','2016-03-25',57,66,1),(160,2935,'a Secar','2017-02-24','2015-11-02',60,4,1),(161,4249,'a Eliminar','2016-11-12','2016-10-11',89,47,1),(162,2910,'a Parir','2017-04-28','2016-05-07',28,19,1),(163,2885,'a Eliminar','2016-11-12','2016-10-04',178,83,1),(164,2896,'a Eliminar','2016-11-12','2016-10-12',135,67,1),(165,2945,'a Eliminar','2016-03-03','2016-10-09',69,26,1),(166,2951,'a Parir','2017-03-30','2015-10-11',82,55,1),(167,2954,'a Cubrir','2017-02-10','2016-09-18',60,12,1),(168,2960,'a Cubrir','2016-11-25','2016-10-11',92,16,1),(169,2965,'a Secar','2017-07-30','2016-10-16',32,88,1),(170,2899,'a Cubrir','2017-03-25','2017-02-08',44,67,1),(171,4257,'a Cubrir','2017-02-10','2015-03-04',80,52,1),(172,4255,'a Parir','2017-03-17','2016-04-09',52,45,1),(173,2849,'a Parir','2017-03-28','2015-11-08',66,11,1),(174,2878,'a Eliminar','2016-11-12','2016-04-29',97,84,1),(175,2872,'a Eliminar','2016-03-03','2016-03-25',57,66,1),(176,2935,'a Secar','2017-02-24','2015-11-02',60,4,1),(177,4249,'a Eliminar','2016-11-12','2016-10-11',89,47,1),(178,2910,'a Parir','2017-04-28','2016-05-07',28,19,1),(179,2885,'a Eliminar','2016-11-12','2016-10-04',178,83,1),(180,2896,'a Eliminar','2016-11-12','2016-10-12',135,67,1),(181,2945,'a Eliminar','2016-03-03','2016-10-09',69,26,1),(182,2951,'a Parir','2017-03-30','2015-10-11',82,55,1),(183,2954,'a Cubrir','2017-02-10','2016-09-18',60,12,1),(184,2960,'a Cubrir','2016-11-25','2016-10-11',92,16,1),(185,2965,'a Secar','2017-07-30','2016-10-16',32,88,1),(186,2899,'a Cubrir','2017-03-25','2017-02-08',44,67,1),(187,4257,'a Cubrir','2017-02-10','2015-03-04',80,52,1),(188,4255,'a Parir','2017-03-17','2016-04-09',52,45,1);
/*!40000 ALTER TABLE `reproduccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumencontrol`
--

DROP TABLE IF EXISTS `resumencontrol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumencontrol` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PredioId` int(11) DEFAULT NULL,
  `Variable` varchar(45) DEFAULT NULL,
  `TercioUno` double(6,2) DEFAULT NULL,
  `TercioDos` double(6,2) DEFAULT NULL,
  `TercioTres` double(6,2) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_RESUMENCONTROL_PREDIO_ID_idx` (`PredioId`),
  CONSTRAINT `FK_PREDIO_RESUMENCONTROL_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumencontrol`
--

LOCK TABLES `resumencontrol` WRITE;
/*!40000 ALTER TABLE `resumencontrol` DISABLE KEYS */;
INSERT INTO `resumencontrol` VALUES (1,1,'Leche (Kg)',25.40,22.10,16.20),(2,1,'Grasa %',3.25,3.42,4.11),(3,1,'Proteina %',3.19,3.23,3.58),(4,1,'Urea mg/dl',390.00,410.00,450.00),(5,1,'RCSx1000',260.00,350.00,420.00);
/*!40000 ALTER TABLE `resumencontrol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumencurvalactancia`
--

DROP TABLE IF EXISTS `resumencurvalactancia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumencurvalactancia` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Dias` int(11) DEFAULT NULL,
  `Otono` int(11) DEFAULT NULL,
  `Primavera` int(11) DEFAULT NULL,
  `PredioId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_CURVAS_PREDIOID_idx` (`PredioId`),
  CONSTRAINT `FK_PREDIO_CURVAS_PREDIOID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumencurvalactancia`
--

LOCK TABLES `resumencurvalactancia` WRITE;
/*!40000 ALTER TABLE `resumencurvalactancia` DISABLE KEYS */;
INSERT INTO `resumencurvalactancia` VALUES (1,20,19,18,1),(2,50,23,26,1),(3,80,25,27,1),(4,110,24,22,1),(5,140,19,16,1),(6,170,22,14,1),(7,200,21,12,1),(8,230,17,9,1),(9,260,14,7,1),(10,290,12,4,1),(11,320,8,5,1);
/*!40000 ALTER TABLE `resumencurvalactancia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumenexistenciacrias`
--

DROP TABLE IF EXISTS `resumenexistenciacrias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumenexistenciacrias` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Variable` varchar(45) DEFAULT NULL,
  `CantidadHembras` int(11) DEFAULT NULL,
  `CantidadMachos` int(11) DEFAULT NULL,
  `PredioId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_EXISTENCIACRIAS_PREDIO_ID_idx` (`PredioId`),
  CONSTRAINT `FK_PREDIO_EXISTENCIACRIAS_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumenexistenciacrias`
--

LOCK TABLES `resumenexistenciacrias` WRITE;
/*!40000 ALTER TABLE `resumenexistenciacrias` DISABLE KEYS */;
INSERT INTO `resumenexistenciacrias` VALUES (1,'Crias',269,178,1),(2,'Más de 24 meses',25,3,1),(3,'18 a 24 meses',65,39,1),(4,'12 a 18 meses',59,28,1),(5,'6 a 12 meses',62,36,1),(6,'3 a 6 meses',30,34,1),(7,'1 a 3 meses',28,38,1);
/*!40000 ALTER TABLE `resumenexistenciacrias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumenexistenciavacas`
--

DROP TABLE IF EXISTS `resumenexistenciavacas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumenexistenciavacas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Variable` varchar(45) DEFAULT NULL,
  `Numero` int(11) DEFAULT NULL,
  `PredioId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_EXISTENCIAVACAS_PREDIOID_idx` (`PredioId`),
  CONSTRAINT `FK_PREDIO_EXISTENCIAVACAS_PREDIOID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumenexistenciavacas`
--

LOCK TABLES `resumenexistenciavacas` WRITE;
/*!40000 ALTER TABLE `resumenexistenciavacas` DISABLE KEYS */;
INSERT INTO `resumenexistenciavacas` VALUES (1,'en Lactancia',280,1),(2,'Preñadas',135,1),(3,'con Cubiertas',90,1),(4,'sin Cubiertas',55,1),(5,'Secas',67,1),(6,'Preñadas',58,1),(7,'sin Preñez',9,1),(8,'Vaquillas',1,1),(9,'Preñadas',45,1),(10,'sin Preñez',30,1);
/*!40000 ALTER TABLE `resumenexistenciavacas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumenlactancia`
--

DROP TABLE IF EXISTS `resumenlactancia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumenlactancia` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PredioId` int(11) DEFAULT NULL,
  `Variable` varchar(45) DEFAULT NULL,
  `NopUno` double(8,2) DEFAULT NULL,
  `NopDos` double(8,2) DEFAULT NULL,
  `NopTres` double(8,2) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_RESUMENLACTANCIA_PREDIO_ID_idx` (`PredioId`),
  CONSTRAINT `FK_PREDIO_RESUMENLACTANCIA_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumenlactancia`
--

LOCK TABLES `resumenlactancia` WRITE;
/*!40000 ALTER TABLE `resumenlactancia` DISABLE KEYS */;
INSERT INTO `resumenlactancia` VALUES (1,1,'Leche (Kg)',25.40,22.10,16.20),(2,1,'Grasa (Kg)',3.25,3.42,4.11),(3,1,'Proteina (Kg)',3.19,3.23,3.58),(4,1,'Grasa (%)',390.00,410.00,450.00),(5,1,'Proteína (%)',260.00,350.00,420.00),(6,1,'Días Lact',325.00,332.00,341.00),(7,1,'Leche (Kg)',6500.00,6850.00,6670.00),(8,1,'Grasa ',254.00,265.00,282.00),(9,1,'Proteína (Kg)',221.00,232.00,241.00);
/*!40000 ALTER TABLE `resumenlactancia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumenreproductivo`
--

DROP TABLE IF EXISTS `resumenreproductivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumenreproductivo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PredioId` int(11) DEFAULT NULL,
  `Variable` varchar(45) DEFAULT NULL,
  `Valor` double(6,2) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_RESUMEREPRODUCTIVO_PREDIO_ID_idx` (`PredioId`),
  CONSTRAINT `FK_PREDIO_RESUMEREPRODUCTIVO_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumenreproductivo`
--

LOCK TABLES `resumenreproductivo` WRITE;
/*!40000 ALTER TABLE `resumenreproductivo` DISABLE KEYS */;
INSERT INTO `resumenreproductivo` VALUES (1,1,'LP1s',78.00),(2,1,'LPP',98.00),(3,1,'LIP',425.00),(4,1,'PS',67.00),(5,1,'ED1P',24.40),(6,1,'EDElim',75.40);
/*!40000 ALTER TABLE `resumenreproductivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `toro`
--

DROP TABLE IF EXISTS `toro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `toro` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(6) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Completo` varchar(55) NOT NULL,
  `PredioId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_TORO_PREDIO_ID` (`PredioId`),
  CONSTRAINT `FK_PREDIO_TORO_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `toro`
--

LOCK TABLES `toro` WRITE;
/*!40000 ALTER TABLE `toro` DISABLE KEYS */;
/*!40000 ALTER TABLE `toro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nick` varchar(20) DEFAULT NULL,
  `Clave` varchar(35) DEFAULT NULL,
  `Email` varchar(60) DEFAULT NULL,
  `Fono` int(11) DEFAULT NULL,
  `Estado` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Nick` (`Nick`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'ANDRES100X','1313','andres100x@gmail.com',965142583,1),(2,'IPETERS','1212','ivan.peters.v@gmail.com',952094319,1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vaca`
--

DROP TABLE IF EXISTS `vaca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vaca` (
  `Id` int(11) NOT NULL,
  `Nacimiento` date DEFAULT NULL,
  `Padre` varchar(6) DEFAULT NULL,
  `Madre` int(11) DEFAULT NULL,
  `Nop` smallint(6) DEFAULT NULL,
  `DIIO` int(11) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `PredioId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PREDIO_VACA_PREDIO_ID` (`PredioId`),
  CONSTRAINT `FK_PREDIO_VACA_PREDIO_ID` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vaca`
--

LOCK TABLES `vaca` WRITE;
/*!40000 ALTER TABLE `vaca` DISABLE KEYS */;
INSERT INTO `vaca` VALUES (1831,'1999-06-17','SOCR  ',1520,8,9371576,'Vc aEli Lac no Prñ',1),(1937,'2001-11-07','SOCR  ',1344,7,9371309,'Vc aEli Lac s/Cb',1),(2191,'2002-08-20','ADVA  ',1633,5,9372491,'Vc Lac s/Cb',1),(2442,'2002-07-03','JUPI  ',2008,6,9371821,'Vc Lac Prñ',1),(2510,'2003-03-12','SOCR  ',1623,6,9371517,'Vc aEli Lac no Prñ',1),(2642,'2004-02-10','RHOM  ',1363,5,9371050,'Vc aEli Lac s/Cb',1),(2657,'2003-10-30','HARM  ',1415,5,9371579,'Vc Lac s/Cb',1),(2664,'2004-09-27','RHOM  ',1747,4,9372484,'Vc aEli Lac s/Cb',1),(2667,'2004-09-15','BADG  ',1549,4,9371308,'Vc Sec Prñ',1),(2678,'2004-09-22','RHOM  ',1357,3,9371825,'Vc Lac EPP Nr s/Cb',1),(2687,'2004-06-02','HARM  ',1375,4,9372979,'Vc aEli Lac s/Cb',1),(2774,'2005-03-24','HARM  ',1570,3,9372571,'Vc Lac EPP Nr no Prñ',1),(2787,'2005-03-12','SUPR  ',1593,4,9371007,'Vc aEli Lac s/Cb',1),(2823,'2005-06-14','HARM  ',2058,3,9372609,'Vc Sec Prñ',1),(2832,'2005-07-12','BADG  ',1387,3,9371285,'Vc aEli Lac EPP Nr Prñ',1),(2843,'2005-04-26','HARM  ',1770,3,9372954,'Vc Lac s/Cb',1),(2849,'2005-09-26','SOCR  ',1720,2,9371272,'Vc aEli Lac s/Cb',1),(2872,'2005-04-05','SUPR  ',1868,2,9371225,'Vc aEli Lac EPP Nr s/Cb',1),(2878,'2005-07-15','HARM  ',2064,2,9372500,'Vc Sec Prñ',1),(2885,'2005-08-11','HARM  ',1699,1,9371274,'Vc Sec EPP Nr Prñ',1),(2896,'2005-09-19','HARM  ',1894,1,9371554,'Vc aEli Lac s/Cb',1),(2899,'2005-09-21','HARM  ',1540,1,9371159,'Vc aEli Lac Prñ',1),(2910,'2005-08-15','ADVA  ',1706,2,9371127,'Vc aEli Lac s/Cb',1),(2935,'2005-06-07','HARM  ',1964,2,9371444,'Vc aEli Lac no Prñ',1),(2945,'2005-06-21','BADG  ',1598,1,9371294,'Vc aEli Lac s/Cb',1),(2951,'2005-10-07','BADG  ',1539,1,9371199,'Vc aEli Lac s/Cb',1),(2954,'2005-08-30','BADG  ',2024,2,9371067,'Vc Sec Prñ',1),(2960,'2005-07-08','ADVA  ',2092,1,9371119,'Vc Lac no Prñ',1),(2965,'2005-09-08','HARM  ',1959,1,9371865,'Vc Lac EPP Nr s/Cb',1),(2993,'2006-02-10','RHOM  ',1768,1,9372926,'Vc Sec Prñ',1),(4218,'2004-05-07','SUPR  ',1736,4,9371037,'Vc aEli Lac s/Cb',1),(4249,'2005-06-11','BADG  ',1841,2,9372527,'Vc Lac Prñ',1),(4255,'2005-09-02','SUPR  ',1642,1,9371548,'Vc Lac EPP Nr no Prñ',1),(4257,'2005-09-06','RHOM  ',1801,1,9371237,'Vc Lac s/Cb',1);
/*!40000 ALTER TABLE `vaca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visualizador`
--

DROP TABLE IF EXISTS `visualizador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visualizador` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) DEFAULT NULL,
  `UsuarioId` int(11) DEFAULT NULL,
  `PredioId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ClienteId` (`ClienteId`),
  KEY `PredioId` (`PredioId`),
  KEY `UsuarioId` (`UsuarioId`),
  CONSTRAINT `visualizador_ibfk_1` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `visualizador_ibfk_2` FOREIGN KEY (`PredioId`) REFERENCES `predio` (`Id`),
  CONSTRAINT `visualizador_ibfk_3` FOREIGN KEY (`UsuarioId`) REFERENCES `usuario` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visualizador`
--

LOCK TABLES `visualizador` WRITE;
/*!40000 ALTER TABLE `visualizador` DISABLE KEYS */;
INSERT INTO `visualizador` VALUES (1,2,1,3),(2,1,1,1),(3,1,2,1),(4,2,1,4),(5,1,1,2);
/*!40000 ALTER TABLE `visualizador` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-17  1:02:14
