﻿using apiCooprinsem.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;

namespace apiCooprinsem.Middlewares
{
    public class TokenValidateMiddleware : JwtBearerOptions
    {

        public TokenValidateMiddleware(IConfigurationSection jwtAppSettingsOptions, SymmetricSecurityKey key)
        {
            AutomaticAuthenticate = true;
            AutomaticChallenge = true;
            TokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingsOptions[nameof(JwtIssuerOptions.Issuer)],
                ValidateAudience = true,
                ValidAudience = jwtAppSettingsOptions[nameof(JwtIssuerOptions.Audience)],
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = key,
                RequireExpirationTime = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };
            //aqui se define lo que se hara dependiendo el estado del token
            Events = new JwtBearerEvents
            {
               
                //este evento se desencadena cuando el token es invalido
                OnChallenge = OnChallengeMiddle

            };
        }
     
        private async Task OnChallengeMiddle(JwtBearerChallengeContext context) {
            var response = new JObject();
            // Override the response status code.
            context.Response.StatusCode = 401;
            response.Add(new JProperty("status", "token invalido o expirado"));
            await context.Response.WriteAsync(
                   JsonConvert.SerializeObject(response));
            context.HandleResponse();
        }
    

    }
}
