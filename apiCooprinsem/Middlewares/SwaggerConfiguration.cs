﻿using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Model;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Annotations;
using Swashbuckle.SwaggerGen.Generator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace apiCooprinsem.Middlewares
{

    public class SwaggerConfiguration : IOperationFilter, ISchemaFilter
    {

        public void Apply(Operation operation, OperationFilterContext context)
        {


            if (operation.Parameters == null)
                operation.Parameters = new List<IParameter>();

            operation.Parameters.Add(new NonBodyParameter
            {
                Name = "Authorization",
                In = "header",
                Description = "JWT Token",
                Required = false,
                Type = "string"
            });

        }

        public void Apply(Schema model, SchemaFilterContext context)
        {
            if (context.SystemType == typeof(Usuario))
            {
                model.Example = new
                {
                    Nick = "string",
                    Rut = "int",
                    Clave = "string",
                    Rol = "Usuario/Administrador",
                    Email = "string",
                    Fono = "int",
                    Estado = "0/1"
                };
            }
        }
    }


}

