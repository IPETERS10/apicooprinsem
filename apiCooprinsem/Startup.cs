﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR.Infrastructure;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using apiCooprinsem.Options;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors.Infrastructure;
using MySQL.Data.EntityFrameworkCore.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Authentication;
using apiCooprinsem.Middlewares;

namespace apiCooprinsem
{
    public class Startup
    {
        private const string SECRET_KEY = "ClaveParaGenerarLosToken";
        public IConfigurationRoot Configuration { get; }
        private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SECRET_KEY));
        public static IConnectionManager ConnectionManager;
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // Add framework services.
            services.AddMvc(c => {
                //autorizamos antes de que llegen las peticiones a los controladores
                var policy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .Build();
                c.Filters.Add(new AuthorizeFilter(policy));
            });
            services.AddAuthorization();
            var jwtAppSettingsOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
            services.Configure<JwtIssuerOptions>(options => {
                options.Issuer = jwtAppSettingsOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingsOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });
            services.AddDbContext<MySqlContext>(options => {
                options.UseMySQL(Configuration.GetConnectionString("DefaultConnection"));
            });
            services.AddCors();
            services.AddSignalR();
            services.AddSwaggerGen(c => {
                //se agregan los heades para el testing en swagger
                c.OperationFilter<SwaggerConfiguration>();
                c.SchemaFilter<SwaggerConfiguration>();
            }
            );
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IServiceProvider serviceProvider)
        {
            
                loggerFactory.AddConsole(Configuration.GetSection("Logging"));
                loggerFactory.AddDebug();
            // guardamos el contexto de forma static para poder utilizarlo desde los HUBS
                ConnectionManager = serviceProvider.GetService<IConnectionManager>();
            var jwtAppSettingsOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
            //implementacion de middleware para validar token
            app.UseJwtBearerAuthentication(new TokenValidateMiddleware(jwtAppSettingsOptions,_signingKey));
       
            app.UseMvc();
            app.UseSignalR();
                app.UseCors("SiteCorsPolicy");
            app.UseSwagger();
            app.UseSwaggerUi();
        }
    }
}
