﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCooprinsem.Options
{
    public class JwtIssuerOptions
    {
        //emisor
        public string Issuer { get; set; }
        //asunto
        public string Subject { get; set; }
        //para quien
        public string Audience { get; set; }
        //desde cuando corre su validez
        public DateTime NotBefore { get; set; } = DateTime.UtcNow;

        public DateTime AssuedAt { get; set; } = DateTime.UtcNow;
        //tiempo de validez
        public TimeSpan ValidFor { get; set; } = TimeSpan.FromDays(1);
        //tiempo de caducidad
        public DateTime Expiration => AssuedAt.Add(ValidFor);
        //tiempo de caducidad refresh token

        public TimeSpan RefreshValidFor { get; set; } = TimeSpan.FromDays(30);
        public DateTime RefreshExpiration => AssuedAt.Add(RefreshValidFor);
        //herramienta encargada de emitirlo
        public Func<Task<string>> JtiGenerator =>
            () => Task.FromResult(Guid.NewGuid().ToString());
        //credenciales , firma
        public SigningCredentials SigningCredentials { get; set; }
    }
}
