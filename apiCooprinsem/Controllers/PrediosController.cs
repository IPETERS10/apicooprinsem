using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using apiCooprinsem.Options;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using Model;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Principal;
using DataAccess;
using DataAccess.Source;
using Newtonsoft.Json.Linq;

namespace apiCooprinsem.Controllers
{
    [Authorize()]
    [Produces("application/json")]
    [Route("api/Predios")]
    public class PrediosController : Controller
    {
        private readonly JsonSerializerSettings _serializerSettings;
        private DAPredios _daPredios; 

        public PrediosController( MySqlContext contextDb)
        { 
            _serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            _daPredios = new DAPredios(contextDb); 
        }

        // GET: api/Predios
        [HttpGet]
        public string Get()
        {  
            var json = JsonConvert.SerializeObject(_daPredios.List(), _serializerSettings);

            return json.ToString();
        }

        // GET: api/Predios/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            var json = JsonConvert.SerializeObject(_daPredios.List(id), _serializerSettings);

            return json.ToString();
        }
        
        // POST: api/Predios
        [HttpPost]
        public void Post([FromBody] string request)
        {
 
        }
        
        // PUT: api/Predios/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
