﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using DataAccessNoSql.Source;
using apiCooprinsem.Options;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using DataAccessNoSql.MongoModels;
using Newtonsoft.Json.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace apiCooprinsem.Controllers
{

    [Route("api/")]
    public class ClientesController : Controller
    {
        private readonly JsonSerializerSettings _serializerSettings;
        private DANoSqlClientes _daClientes;
        public ClientesController()
        {
            _serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            _daClientes = new DANoSqlClientes();
        }
        // GET: api/clientes
        //[HttpGet]
        //public async Task<IActionResult> Get()
        //{
        //    var usuarios = await _daClientes.ListarClientesPorUsuario();
        //    if (usuarios == null)
        //    {
        //        return new NotFoundObjectResult(JsonConvert.SerializeObject(new JObject(
        //          new JProperty("error", "ningun usuario tiene clientes asociados"))));
        //    }
        //    return new OkObjectResult(JsonConvert.SerializeObject(usuarios, _serializerSettings));
        //}
        

        //devuelve un cliente especifico
        [HttpGet]
        [Route("cliente/{rut}")]
        public string Get(int rut)
        {
            //select * from usaurios asociados a este rut de cliente = id 
            return "value";
        }

      
        [Route("clientes/{rutCliente}/opciones/{rutUser}")]
        [HttpPost] // para que funcione los parametros deben llamarse igual sino  hay que hacer {cliente=id, usaurio = user}
        public async Task<IActionResult> AsignaPrivilegios([FromBody] MonOpciones opciones, int rutCliente, int rutUser) {
            var metodo = await _daClientes.AsignaPrivilegiosAUsuarios();
            return Ok();
        }
        //captura el rut de cliente {id}, pero lista los usaurios asociados solo a ese predio {predio}
        [Route("clientes/{id}/predio/{predio}")]
        [HttpGet]
        public string GetPredios(int id, int predio)
        {
            var usuarios = _daClientes.ListarPrediosPorUsuario();
            return "cliente " + id +"  ; predio: " + predio;
        }

        [HttpPost]
        [Route("usuariosfile")]
        public async Task<IActionResult> PostFileAsync(IList<IFormFile> files)
        {
            Console.WriteLine("usuariosfile");
            //en nombre del post del cliente debe ser el mismo que el parametro "files" , no es necesario especificar content type
            long size = files.Sum(f => f.Length);
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    
                }
            }

            return  Ok(new { count = files.Count, size });
        }
       
    }
}
