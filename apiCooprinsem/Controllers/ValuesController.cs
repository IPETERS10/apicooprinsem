﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Model;
using Microsoft.AspNetCore.SignalR;
using apiCooprinsem;
using Hubs;
using DataAccess;

namespace APIRestCooprinsem.Controllers
{
    [Authorize()]
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        [Produces("application/json",Type = typeof(Usuario))] 
        public async Task<IActionResult> Post([FromBody] Usuario value) {
            if (ModelState.IsValid) {
                //se trae el contexto del hub hacia la api
                var context = Startup.ConnectionManager.GetHubContext<MyHub>();
                //ejecuta el metodo del cliente
              await  context.Clients.All.logOn(value.Nick + " " + value.Clave);
                return Ok();
            }
            else
                return BadRequest(ModelState);
        }
  

        // PUT api/values/5
        [HttpPut("{id}")] 
        public void Put(int id, [FromBody]string value)
        {

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

        }
        
    }
}
