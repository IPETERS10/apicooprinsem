﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using DataAccessNoSql.Source;
using apiCooprinsem.Options;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using DataAccessNoSql.MongoModels;
using Newtonsoft.Json.Linq;
using DataAccess.Source;
using DataAccess;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace apiCooprinsem.Controllers
{

    [Route("api/")]
    public class OpcionesController : Controller
    {
        private readonly JsonSerializerSettings _serializerSettings;
        private DAOpciones _daOpciones;
        public OpcionesController(MySqlContext contextDb)
        {
            _serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            _daOpciones = new DAOpciones(contextDb);
        }

        [HttpPost]
        [Route("opciones/{rut}/{predio}/")]
        public void Post([FromBody] string request)
        {

        }
    }
}
