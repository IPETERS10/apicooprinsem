using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using apiCooprinsem.Options;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using Model;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Principal;
using DataAccess;
using DataAccess.Source;
using Newtonsoft.Json.Linq;
using DataAccessNoSql.Source;
using DataAccessNoSql.MongoModels;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Builder;
using System.Text;
using apiCooprinsem.Middlewares;
using Swashbuckle.SwaggerGen.Annotations;
using static apiCooprinsem.Middlewares.SwaggerConfiguration;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Generator;

namespace apiCooprinsem.Controllers
{
    [Route("api/[controller]")]
    public class JwtController : Controller
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        private DAUsuarios _daUsuarios;
        private const string SCHEMA_ROL = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
        public JwtController(IOptions<JwtIssuerOptions> jwtOptions, MySqlContext contextDb)
        {
            _jwtOptions = jwtOptions.Value;
            ThrowIfInvalidOptions(_jwtOptions); //valida que todas las opciones esten correctas
            _serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            _daUsuarios = new DAUsuarios(contextDb);
        }
      
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody]Login usuario)
        {
            //consulta a mysql el usuario
            Usuario user = await GetUsuarioFromDatabaseAsync(new Usuario() {Nick = usuario.Nick,Clave = usuario.Clave });
            if (user == null)
            {
                return NotFound(JsonConvert.SerializeObject(new JObject {
                    new JProperty("error", "Usuario no encontrado") }, _serializerSettings));
            }
            return await GenerateToken(user.Rut.ToString(), rol: user.Rol);
        }
   
        private async Task<Usuario> GetUsuarioFromDatabaseAsync(Usuario usuario) {
           return await _daUsuarios.GetUsuarioFromDatabase(usuario);
        }

       
        private async Task<IActionResult> GenerateToken(string rut, string rol) {
            var claims = new List<Claim>()
            {
               
             new Claim(JwtRegisteredClaimNames.Sub,rut),//pasamos como claim el nombre de usuario
             new Claim(JwtRegisteredClaimNames.Jti,await _jwtOptions.JtiGenerator()),
             //tranformamos la fecha a la fecha standard de toda la vida en segundos
             new Claim(JwtRegisteredClaimNames.Iat,ToUnixEpochDate(_jwtOptions.AssuedAt).ToString()
             ,ClaimValueTypes.Integer64)
            };

            var response = new JObject
            { 
                //tranformamos el jwtsecuritytoken en un jwt string
                 new JProperty("access_token",await GenerateTokenAccess(rut,rol)),
                 new JProperty("access_token_expires_in",_jwtOptions.ValidFor.TotalSeconds),
                 new JProperty("refresh_token",await GenerateTokenRefresh(rut,rol)),
                 new JProperty("refresh_token_expires_in",_jwtOptions.RefreshValidFor.TotalSeconds),
            };
            return new OkObjectResult(JsonConvert.SerializeObject(response, _serializerSettings));
        }
        
        private async Task<string> GenerateTokenRefresh(string rut, string rol)
        {
            var claims = new List<Claim>()
            {
             new Claim(JwtRegisteredClaimNames.Sub,rut),//pasamos como claim el nombre de usuario
             new Claim(JwtRegisteredClaimNames.Jti,await _jwtOptions.JtiGenerator()),
             //tranformamos la fecha a la fecha standard de toda la vida en segundos
             new Claim(JwtRegisteredClaimNames.Iat,ToUnixEpochDate(_jwtOptions.AssuedAt).ToString()
             ,ClaimValueTypes.Integer64)
            };


            var refreshJwt = new JwtSecurityToken(
               issuer: _jwtOptions.Issuer,
               audience: _jwtOptions.Audience,
               claims: claims,
               notBefore: _jwtOptions.NotBefore,
               expires: _jwtOptions.RefreshExpiration,
               signingCredentials: _jwtOptions.SigningCredentials
               );
            return new JwtSecurityTokenHandler().WriteToken(refreshJwt);
        }
        private async Task<string> GenerateTokenAccess(string rut, string rol) {
            var claims = new List<Claim>()
            {
                //SE DEFINE EL ROL QUE TENDRA EL TOKEN EN EL SISTEMA
             new Claim(ClaimsIdentity.DefaultRoleClaimType, rol),
             new Claim(JwtRegisteredClaimNames.Sub,rut),//pasamos como claim el nombre de usuario
             new Claim(JwtRegisteredClaimNames.Jti,await _jwtOptions.JtiGenerator()),
             
             //tranformamos la fecha a la fecha standard de toda la vida en segundos
             new Claim(JwtRegisteredClaimNames.Iat,ToUnixEpochDate(_jwtOptions.AssuedAt).ToString()
             ,ClaimValueTypes.Integer64)
            };

            var accessJwt = new JwtSecurityToken(
          issuer: _jwtOptions.Issuer,
          audience: _jwtOptions.Audience,
          claims: claims,
          notBefore: _jwtOptions.NotBefore,
          expires: _jwtOptions.Expiration,
          signingCredentials: _jwtOptions.SigningCredentials
          );
            return new JwtSecurityTokenHandler().WriteToken(accessJwt);
        }
        [Route("refresh")]
        [Authorize()]
        [HttpPut]
        public async Task<IActionResult> RefreshToken()
        {
            var authorization = HttpContext.Request.Headers["Authorization"].SingleOrDefault();
            var token = authorization.Substring(authorization.IndexOf(' ') + 1);
            var jwt = new JwtSecurityTokenHandler().ReadToken(token) as JwtSecurityToken;

            var result = new JObject(
               new JProperty("message", "refresh exitoso"),
               new JProperty("rut", jwt.Subject),
               new JProperty("new_token", await GenerateTokenAccess(jwt.Subject,
               jwt.Claims.Where(e => e.Type == SCHEMA_ROL).FirstOrDefault().Value))
               );
            return new OkObjectResult(JsonConvert.SerializeObject(result, _serializerSettings));
        }
        private static void ThrowIfInvalidOptions(JwtIssuerOptions options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));
            if (options.ValidFor <= TimeSpan.Zero)
            {
                throw new ArgumentException("Debe ser un valor distinto de cero",
                    nameof(JwtIssuerOptions.ValidFor));
            }
            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.SigningCredentials));
            }
            if (options.JtiGenerator == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.JtiGenerator));
            }
        }
        private static long ToUnixEpochDate(DateTime date) => (long)Math.Round((date.ToUniversalTime() -
            new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);

        public struct Login
        {
            public string Nick { get; set; }
            public string Clave { get; set; }
        }
    }
}