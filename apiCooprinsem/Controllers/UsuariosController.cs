﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using DataAccessNoSql.Source;
using apiCooprinsem.Options;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using DataAccessNoSql.MongoModels;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Primitives;
using System.Security.Claims;
using Model;
using DataAccess.Source;
using DataAccess;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace apiCooprinsem.Controllers
{

    [Route("api/")]
    public class UsuariosController : Controller
    {
        private readonly JsonSerializerSettings _serializerSettings;
        private DANoSqlUsuarios _daUsuariosMongo;
        private DAUsuarios _daUsuarios;
        public UsuariosController(MySqlContext contextDb)
        {
            _serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            _daUsuariosMongo = new DANoSqlUsuarios();
            _daUsuarios = new DAUsuarios(contextDb);
        }

        #region Peticiones Get
        #region lista todos los usuarios(api/usuarios)
        [HttpGet]
        [Produces("application/json")]
        [Authorize(Roles = "Usuario")] //aqui va administrador
        [Route("usuarios")]
        public async Task<IActionResult> Get()
        {
            var usuarios = await _daUsuariosMongo.ListarUsuarios();
            if (usuarios == null)
            {
                return new NotFoundObjectResult(JsonConvert.SerializeObject(new JObject(
                  new JProperty("error", "ningun usuario tiene clientes asociados"))));
            }
            return new OkObjectResult(JsonConvert.SerializeObject(usuarios, _serializerSettings));
        }
        #endregion

        #region trae todos los clientes del propietario del token (api/usuario/clientes)
        [Route("usuario/clientes")]
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetClientesDelUsuario()
        {
            //trae el sub (rut) del token
            var rut = (HttpContext.User.Identity as ClaimsIdentity).Claims.ElementAt(1).Value;
            var clientes = await _daUsuariosMongo.ListarClientesPorUsuario(int.Parse(rut));
            if (clientes == null)
            {
                return new NotFoundObjectResult(JsonConvert.SerializeObject(new JObject(
                  new JProperty("error", "este usuario no tiene clientes"))));
            }
            return new OkObjectResult(JsonConvert.SerializeObject(clientes, _serializerSettings));
        }
        #endregion

        #region trae un cliente del propietario del token (api/usuario/clientes/{rut})
        [Route("usuario/clientes/{rutCliente}")]
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetClienteDelUsuario(int rutCliente)
        {

            //trae el sub (rut) del token
            var rut = (HttpContext.User.Identity as ClaimsIdentity).Claims.ElementAt(1).Value;
            var cliente = await _daUsuariosMongo.GetClienteDelUsuario(int.Parse(rut), rutCliente);
            if (cliente == null)
            {
                return new NotFoundObjectResult(JsonConvert.SerializeObject(new JObject(
                  new JProperty("error", "este cliente no existe"))));
            }
            return new OkObjectResult(JsonConvert.SerializeObject(cliente, _serializerSettings));
        }
        #endregion

        #region trae todos los predios de cliente un cliente especifico y que el propietario del token (api/usuario/clientes/{rutCliente}/predios)
        [Route("usuario/clientes/{rutCliente}/predios")]
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetPrediosDelCliente(int rutCliente)
        {
            var rut = (HttpContext.User.Identity as ClaimsIdentity).Claims.ElementAt(1).Value;
            var predios = await _daUsuariosMongo.GetPrediosDelCliente(int.Parse(rut), rutCliente);
            if (predios == null)
            {
                return new NotFoundObjectResult(JsonConvert.SerializeObject(new JObject(
                  new JProperty("error", "este cliente no posee predios"))));
            }
            return new OkObjectResult(JsonConvert.SerializeObject(predios, _serializerSettings));
        }
        #endregion

        #region trae todos los datos del usuario poseedor del token (api/usuario)
        [Route("usuario/")]
        [Produces("application/json")]
        [HttpGet]
        public async Task<IActionResult> GetUsuario()
        {
            //trae el sub (rut) del token
            var rut = (HttpContext.User.Identity as ClaimsIdentity).Claims.ElementAt(1).Value;
            var usuario = await _daUsuariosMongo.GetUsuario(int.Parse(rut));
            if (usuario == null)
            {
                return new NotFoundObjectResult(JsonConvert.SerializeObject(new JObject(
                  new JProperty("error", "este usuario no existe"))));
            }
            return new OkObjectResult(JsonConvert.SerializeObject(usuario, _serializerSettings));
        }

        #endregion

        #endregion

        #region Peticiones Post

        #region Agregar Usuario
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [Authorize(Roles = "Usuario")]
        [Route("usuario")]
        public async Task<IActionResult> AddUser([FromBody]Usuario usuario)
        {
            //este metodo solo funciona si el token es valido y si su rol es administrador
            if (ModelState.IsValid)
            {
                var user = new MonUsuarios() { Rut = usuario.Rut, Nick = usuario.Nick, Clientes = null };
                //inserta el usuario en mysql y luego los datos basicos en mongo
                if (await _daUsuarios.AddUser(usuario) && await _daUsuariosMongo.AddUser(user))
                {
                    return new OkObjectResult(JsonConvert.SerializeObject(new JObject(
                     new JProperty("success", "usuario agregado con exito"))));
                }

            }

            return new BadRequestObjectResult(JsonConvert.SerializeObject(new JObject(
                  new JProperty("error", "no se pudo agregar este usuario"))));
        }
        #endregion

        #endregion

        #region Peticiones Put

        #endregion

        #region Peticiones Delete

        #endregion
    }
}
